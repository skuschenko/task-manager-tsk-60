package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.dto.ITaskDTORepository;
import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.configuration.ServerConfiguration;
import com.tsc.skuschenko.tm.dto.TaskDTO;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.repository.dto.TaskDTORepository;
import com.tsc.skuschenko.tm.service.PropertyService;
import org.hibernate.UnresolvableObjectException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

public class TaskRepositoryTest {

    @NotNull
    private static AnnotationConfigApplicationContext context;

    @NotNull
    private static  EntityManager entityManager;

    @BeforeClass
    public static void before() {
        context =
                new AnnotationConfigApplicationContext(
                        ServerConfiguration.class
                );
        entityManager = context.getBean(EntityManager.class);
    }

    @AfterClass
    public static void after() {
        entityManager.close();
    }

    @Test
    public void testChangeStatusById() {
        @NotNull final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        task.setStatus("status1");
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskRepository.findById(task.getId());
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals("status1", taskFind.getStatus());
    }

    @Test
    public void testChangeStatusByName() {
        @Nullable final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        task.setStatus("status1");
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskRepository.findOneByName(
                        task.getUserId(), task.getName()
                );
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals("status1", taskFind.getStatus());
    }

    @Test(expected = UnresolvableObjectException.class)
    public void testClear() {
        @Nullable final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        entityManager.getTransaction().begin();
        taskRepository.clearAllTasks();
        entityManager.getTransaction().commit();
        entityManager.refresh(task);
    }

    @Test
    public void testCompleteById() {
        @NotNull final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        task.setStatus(Status.COMPLETE.getDisplayName());
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskRepository.findById(task.getId());
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testCompleteByIndex() {
        @NotNull final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        task.setStatus(Status.COMPLETE.getDisplayName());
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskRepository.findOneByIndex(task.getUserId(), 0);
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testCompleteByName() {
        @NotNull final TaskDTO task = testTaskModel();
        task.setStatus(Status.COMPLETE.getDisplayName());
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(task.getName());
        @Nullable final TaskDTO taskFind =
                taskRepository.findOneByName(
                        task.getUserId(), task.getName()
                );
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testCreate() {
        @NotNull final TaskDTO task = testTaskModel();
        testRepository(task);
    }

    @Test
    public void testFindOneById() {
        @Nullable final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        @Nullable final TaskDTO taskFind =
                taskRepository.findById(task.getId());
        Assert.assertNotNull(taskFind);
    }

    @Test
    public void testFindOneByIndex() {
        @Nullable final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        @Nullable final TaskDTO taskFind =
                taskRepository.findOneByIndex(task.getUserId(), 0);
        Assert.assertNotNull(taskFind);
    }

    @Test
    public void testFindOneByName() {
        @Nullable final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        @Nullable final TaskDTO taskFind =
                taskRepository.findOneByName(
                        task.getUserId(), task.getName()
                );
        Assert.assertNotNull(taskFind);
    }

    @Test(expected = UnresolvableObjectException.class)
    public void testRemoveOneById() {
        @Nullable final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        entityManager.getTransaction().begin();
        taskRepository.removeOneById(task.getUserId(), task.getId());
        entityManager.getTransaction().commit();
        entityManager.refresh(task);
    }

    @NotNull
    private ITaskDTORepository testRepository(@NotNull final TaskDTO task) {
        @NotNull final ITaskDTORepository taskRepository =
               context.getBean(TaskDTORepository.class,entityManager);
        @Nullable final List<TaskDTO> taskDTOList = taskRepository
                .findAllWithUserId(task.getUserId());
        Assert.assertTrue(Optional.ofNullable(taskDTOList).isPresent());
        entityManager.getTransaction().begin();
        taskRepository.clearAllTasks();
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        taskRepository.add(task);
        entityManager.getTransaction().commit();
        @Nullable final TaskDTO taskById =
                taskRepository.findById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(taskById.getId(), task.getId());
        return taskRepository;
    }

    @Test
    public void testStartById() {
        @NotNull final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        task.setStatus(Status.IN_PROGRESS.getDisplayName());
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskRepository.findById(task.getId());
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.IN_PROGRESS.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testStartByIndex() {
        @NotNull final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        task.setStatus(Status.IN_PROGRESS.getDisplayName());
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskRepository.findOneByIndex(task.getUserId(), 0);
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.IN_PROGRESS.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testStartByName() {
        @NotNull final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        task.setStatus(Status.COMPLETE.getDisplayName());
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskRepository.findOneByName(
                        task.getUserId(), task.getName()
                );
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), taskFind.getStatus()
        );
    }

    @NotNull
    private TaskDTO testTaskModel() {
        @Nullable final TaskDTO task = new TaskDTO();
        task.setUserId("72729b26-01dd-4314-8d8c-40fb8577c6b5");
        task.setName("name1");
        task.setDescription("des1");
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getUserId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("name1", task.getName());
        return task;
    }

    @Test
    public void testUpdateOneById() {
        @NotNull final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        task.setName("name2");
        task.setDescription("des2");
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        @Nullable final TaskDTO taskFind =
                taskRepository.findById(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        Assert.assertEquals("name2", taskFind.getName());
        Assert.assertEquals("des2", taskFind.getDescription());
    }

    @Test
    public void testUpdateOneByIndex() {
        @NotNull final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        task.setName("name2");
        task.setDescription("des2");
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        @Nullable final TaskDTO taskFind =
                taskRepository.findOneByIndex(task.getUserId(), 0);
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        Assert.assertEquals("name2", taskFind.getName());
        Assert.assertEquals("des2", taskFind.getDescription());
    }

}
