package com.tsc.skuschenko.tm.service.dto;

import com.tsc.skuschenko.tm.api.repository.dto.IProjectDTORepository;
import com.tsc.skuschenko.tm.api.repository.dto.ITaskDTORepository;
import com.tsc.skuschenko.tm.api.service.dto.IProjectTaskDTOService;
import com.tsc.skuschenko.tm.dto.ProjectDTO;
import com.tsc.skuschenko.tm.dto.TaskDTO;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.service.AbstractService;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
public final class ProjectTaskDTOService extends AbstractService
        implements IProjectTaskDTOService {

    @NotNull
    private final String PROJECT_ID = "project id";

    @NotNull
    private final String TASK_ID = "task id";

    @NotNull
    @Autowired
    private IProjectDTORepository projectRepository;

    @NotNull
    @Autowired
    private ITaskDTORepository taskRepository;

    @NotNull
    @Override
    @Transactional
    public TaskDTO bindTaskByProject(
            @NotNull final String userId, @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        Optional.ofNullable(projectId)
                .orElseThrow(() -> new EmptyIdException(PROJECT_ID));
        Optional.ofNullable(taskId)
                .orElseThrow(() -> new EmptyIdException(TASK_ID));
        Optional.ofNullable(projectRepository.findOneById(userId, projectId))
                .orElseThrow(ProjectNotFoundException::new);
        @NotNull final TaskDTO task = Optional.ofNullable(
                taskRepository.findOneById(userId, taskId)
        ).orElseThrow(TaskNotFoundException::new);
        task.setProjectId(projectId);
        taskRepository.update(task);
        return task;
    }

    @Override
    public void clearProjects(@NotNull final String userId) {
        @Nullable final List<ProjectDTO> projects =
                projectRepository.findAllWithUserId(userId);
        @Nullable final Optional<List<ProjectDTO>> findProjects =
                Optional.ofNullable(projects).filter(item -> item.size() != 0);
        findProjects.ifPresent(items -> items.forEach(item ->
                deleteProjectById(userId, item.getId())));
    }

    @Nullable
    @Override
    @Transactional
    public ProjectDTO deleteProjectById(
            @NotNull final String userId, @Nullable final String projectId
    ) {
        Optional.ofNullable(projectId)
                .orElseThrow(() -> new EmptyIdException(PROJECT_ID));
        @Nullable final List<TaskDTO> projectTasks =
                findAllTaskByProjectId(userId, projectId);
        @Nullable final Optional<List<TaskDTO>> tasks =
                Optional.ofNullable(projectTasks)
                        .filter(item -> item.size() != 0);
        tasks.ifPresent(items -> items.forEach(taskRepository::remove));
        @Nullable final ProjectDTO project =
                projectRepository.findOneById(userId, projectId);
        projectRepository.removeOneById(userId, projectId);
        return project;
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllTaskByProjectId(
            @NotNull final String userId, @Nullable final String projectId
    ) {
        Optional.ofNullable(projectId)
                .orElseThrow(() -> new EmptyIdException(PROJECT_ID));
        return taskRepository.findAllTaskByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO unbindTaskFromProject(
            @NotNull final String userId, @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        Optional.ofNullable(projectId)
                .orElseThrow(() -> new EmptyIdException(PROJECT_ID));
        Optional.ofNullable(taskId)
                .orElseThrow(() -> new EmptyIdException(TASK_ID));
        @NotNull final TaskDTO task = Optional.ofNullable(
                taskRepository.findOneById(userId, taskId)
        ).orElseThrow(TaskNotFoundException::new);
        task.setProjectId(null);
        taskRepository.update(task);
        return task;
    }

}
