package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.service.ILogService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.*;

@Service
public final class LogService implements ILogService {

    @NotNull
    private final Logger commands = Logger.getLogger("COMMANDS");

    @NotNull
    private final Logger errors = Logger.getLogger("ERRORS");

    @NotNull
    private final LogManager manager = LogManager.getLogManager();

    @NotNull
    private final Logger messages = Logger.getLogger("MESSAGES");

    @NotNull
    private final Logger root = Logger.getLogger("");

    {
        init();
        registry(commands, "./commands.txt", false);
        registry(messages, "./messages.txt", true);
        registry(errors, "./errors.txt", true);
    }

    @Override
    public void command(@Nullable final String message) {
        Optional.ofNullable(message).ifPresent(commands::info);
    }

    @Override
    public void debug(@Nullable final String message) {
        Optional.ofNullable(message).ifPresent(messages::fine);
    }

    @Override
    public void error(@Nullable final Exception e) {
        Optional.ofNullable(e)
                .ifPresent(item ->
                        errors.log(Level.SEVERE, item.getMessage(), item)
                );
    }

    @NotNull
    private ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    @Override
    public void info(@Nullable final String message) {
        Optional.ofNullable(message).ifPresent(messages::info);
    }

    private void init() {
        try {
            manager.readConfiguration(
                    LogService.class.getResourceAsStream("/logger.properties")
            );
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

    public void registry(
            @NotNull final Logger logger, @NotNull final String filename,
            final boolean isConsole
    ) {
        try {
            if (isConsole) logger.addHandler(getConsoleHandler());
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(filename));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

}
