package com.tsc.skuschenko.tm.service.dto;

import com.tsc.skuschenko.tm.api.repository.dto.ISessionDTORepository;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.api.service.dto.ISessionDTOService;
import com.tsc.skuschenko.tm.api.service.dto.IUserDTOService;
import com.tsc.skuschenko.tm.dto.SessionDTO;
import com.tsc.skuschenko.tm.dto.UserDTO;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.repository.dto.SessionDTORepository;
import com.tsc.skuschenko.tm.util.HashUtil;
import com.tsc.skuschenko.tm.util.SignatureUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Optional;

@Getter
@Service
@NoArgsConstructor
@Transactional
public class SessionDTOService extends AbstractDTOBusinessService<SessionDTO>
        implements ISessionDTOService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ISessionDTORepository sessionRepository;

    @NotNull
    @Autowired
    private IUserDTOService userDTOService;

    @Override
    @Nullable
    public UserDTO checkDataAccess(
            @Nullable final String login,
            @Nullable final String password) {
        if (!Optional.ofNullable(login).isPresent()) return null;
        if (!Optional.ofNullable(password).isPresent()) return null;
        @Nullable final UserDTO user = userDTOService.findByLogin(login);
        if (!Optional.ofNullable(user).isPresent()) return null;
        @Nullable final String secret = propertyService.getPasswordSecret();
        @Nullable final Integer iteration =
                propertyService.getPasswordIteration();
        @Nullable final String passwordHash =
                HashUtil.salt(secret, iteration, password);
        if (!Optional.ofNullable(passwordHash).isPresent() ||
                !passwordHash.equals(user.getPasswordHash())) return null;
        return user;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void close(@NotNull final SessionDTO session)
            throws AccessForbiddenException {
        sessionRepository.remove(session);
    }

    @Override
    public ISessionDTORepository getSessionRepository() {
        @NotNull final EntityManager entityManager =
                context.getBean(EntityManager.class);
        return context.getBean(SessionDTORepository.class, entityManager);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public SessionDTO open(
            @Nullable final String login, @Nullable final String password
    ) {
        @Nullable final UserDTO user = checkDataAccess(login, password);
        Optional.ofNullable(user).orElseThrow(AccessForbiddenException::new);
        @NotNull final SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.add(session);
        return sign(session);
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO sign(@Nullable final SessionDTO session) {
        if (!Optional.ofNullable(session).isPresent()) return null;
        session.setSignature(null);
        @Nullable final String salt = propertyService.getSessionSalt();
        if (!Optional.ofNullable(salt).isPresent()) return null;
        @Nullable final String cycle = propertyService.getSessionCycle();
        if (!Optional.ofNullable(cycle).isPresent()) return null;
        final int iteration = Integer.parseInt(cycle);
        @Nullable final String signature =
                SignatureUtil.sign(session, salt, iteration);
        session.setSignature(signature);
        return session;
    }

    @Override
    public void validate(@Nullable SessionDTO session, @Nullable Role role)
            throws AccessForbiddenException {
        Optional.ofNullable(role)
                .orElseThrow(AccessForbiddenException::new);
        validate(session);
        @Nullable final String userId = session.getUserId();
        Optional.ofNullable(userId)
                .orElseThrow(AccessForbiddenException::new);
        @Nullable final UserDTO user = userDTOService.findById(userId);
        Optional.ofNullable(user)
                .orElseThrow(AccessForbiddenException::new);
        Optional.ofNullable(user.getRole())
                .orElseThrow(AccessForbiddenException::new);
        if (!role.getDisplayName().equals(user.getRole()))
            throw new AccessForbiddenException();
    }

    @Override
    public void validate(@Nullable final SessionDTO session)
            throws AccessForbiddenException {
        Optional.ofNullable(session)
                .orElseThrow(AccessForbiddenException::new);
        Optional.ofNullable(session.getSignature())
                .orElseThrow(AccessForbiddenException::new);
        Optional.ofNullable(session.getUserId())
                .orElseThrow(AccessForbiddenException::new);
        Optional.ofNullable(session.getTimestamp())
                .orElseThrow(AccessForbiddenException::new);
        @Nullable final SessionDTO tempSession = session.clone();
        Optional.ofNullable(tempSession)
                .orElseThrow(AccessForbiddenException::new);
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final SessionDTO signSession = sign(tempSession);
        Optional.ofNullable(signSession)
                .orElseThrow(AccessForbiddenException::new);
        @Nullable final String signatureTarget = signSession.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessForbiddenException();
    }

}
